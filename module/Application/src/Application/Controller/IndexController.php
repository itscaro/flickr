<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Zend\Db;
use Zend;
use Itscaro;

class IndexController extends AbstractActionController {

    public function __construct()
    {
        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Socket',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $httpClient = new \Zend\Http\Client('', $config);

        \ZendOAuth\Consumer::setHttpClient($httpClient);
    }

    public function indexAction()
    {
        $flashMessenger = $this->flashMessenger();
        if ($flashMessenger->hasMessages()) {
            $view['messages'] = $flashMessenger->getMessages();
        }

        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");

        if ($sessionManager->getStorage()->flickr_access_token) {
            $view['isLogged'] = true;
            $flashMessenger->addMessage('logged');
            return $this->redirect()->toRoute('application/default', array('controller' => 'index',
                        'action' => 'logged'));
        } else {
            $view['isLogged'] = false;
        }

        return new ViewModel($view);
    }

    public function loggedAction()
    {
        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");

        if ($sessionManager->getStorage()->flickr_access_token === null) {
            return $this->redirect()->toRoute('application');
        }

        $token = unserialize($sessionManager->getStorage()->flickr_access_token);
        /* @var $token \ZendOAuth\Token\Access */

        //DB
        $user = $this->getUserTable()->getByUserNsid($token->getParam('user_nsid'));
        $accessToken = new \ZendOAuth\Token\Access();
        $accessToken->setToken($user->oauth_token)
                ->setTokenSecret($user->oauth_token_secret);

        $_startTime = microtime(true);

        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $flickr = new Itscaro\Service\Flickr\Flickr($this->_config['flickr']['oauth'], $config);
        $flickr->setAccessToken($accessToken);

        // Get tags
        $responses = $flickr->tagGetListUser($user->user_nsid)->dispatch();

        $tags = array();
        foreach ($responses as $response) {
            foreach ($response['who']['tags']['tag'] as $_tag) {
                $tags[$_tag['_content']] = $_tag['_content'];
            }
        }
        asort($tags);

        // Get sets
        $responses = $flickr->photosetGetList($user->user_nsid)->dispatch();

        $sets = array();
        foreach ($responses as $response) {
            foreach ($response['photosets']['photoset'] as $_set) {
                $sets[$_set['id']] = $_set['title']['_content'];
            }
        }
        asort($sets);

        $form = new \Application\Form\Flickr();
        $form->get('tags')->setValueOptions($tags);
        $form->get('set')->setValueOptions($sets);

        return new ViewModel(array('form' => $form));
    }

    /**
     *
     * @return \Application\Model\UserTable
     */
    public function getUserTable()
    {
        return $this->getServiceLocator()->get('Application\Model\UserTable');
    }

}
