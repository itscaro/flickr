<?php

namespace Application\Controller;

use Itscaro;
use Zend;
use Zend\Db;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class FlickrController extends AbstractActionController {

    public function __construct()
    {
    }

    public function authAction()
    {
        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Socket',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $httpClient = new \Zend\Http\Client('', $config);
        \ZendOAuth\Consumer::setHttpClient($httpClient);
        
        // Oauth client
        $consumer = new \ZendOAuth\Consumer($this->_config['flickr']['oauth']);

        $consumer->getRequestToken();

        // persist the token to storage
        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");
        /* @var $sessionManager \Zend\Session\SessionManager */
        $sessionManager->getStorage()->flickr_request_token = serialize($consumer->getLastRequestToken());

        // redirect the user
        $consumer->redirect(array(
            'perms' => 'write'
        ));

//        $adapter = new Db\Adapter\Adapter(array(
//            'driver' => 'Pdo_Sqlite',
//            'database' => dirname(__DIR__) . "/sql.lite",
//        ));
//        $adapter->query("INSERT INTO auth ");
    }

    public function callbackAction()
    {
        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Socket',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $httpClient = new \Zend\Http\Client('', $config);
        \ZendOAuth\Consumer::setHttpClient($httpClient);
        
        // Oauth client
        $consumer = new \ZendOAuth\Consumer($this->_config['flickr']['oauth']);

        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");
        /* @var $sessionManager \Zend\Session\SessionManager */
        if (!empty($_GET) && $sessionManager->getStorage()->flickr_request_token) {
            $token = $consumer->getAccessToken($_GET, unserialize($sessionManager->getStorage()->flickr_request_token));
var_dump($token);
            // Save to db
            $user = new \Application\Model\User();
            $user->exchangeArray(array(
                'user_nsid' => $token->getParam('user_nsid'),
                'username' => $token->getParam('username'),
                'fullname' => $token->getParam('fullname'),
                'oauth_token' => $token->getParam('oauth_token'),
                'oauth_token_secret' => $token->getParam('oauth_token_secret'),
            ));
            $userTable = $this->getUserTable();
            $userTable->save($user);

//            $db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
//            $result = $db->query('SELECT * FROM user WHERE user_nsid = :user_nsid')->execute(array(
//                'user_nsid' => $token->getParam('user_nsid'),
//            ));
//            if ($result->count() == 0) {
//                $insertResult = $db->query('INSERT INTO user (user_nsid, username, fullname, oauth_token, oauth_token_secret) VALUES (:user_nsid, :username, :fullname, :oauth_token, :oauth_token_secret)')->execute(array(
//                    'user_nsid' => $token->getParam('user_nsid'),
//                    'username' => $token->getParam('username'),
//                    'fullname' => $token->getParam('fullname'),
//                    'oauth_token' => $token->getParam('oauth_token'),
//                    'oauth_token_secret' => $token->getParam('oauth_token_secret'),
//                ));
//
//                if ($insertResult->getAffectedRows() != 1) {
//                    $this->_logger->err('Error while saving user to database', $token);
//                }
//            }

            $sessionManager->getStorage()->flickr_access_token = serialize($token);

            // Now that we have an Access Token, we can discard the Request Token
            $sessionManager->getStorage()->flickr_request_token = null;

            return $this->redirect()->toUrl('/');
        } else {
            // Mistaken request? Some malfeasant trying something?
            $this->flashMessenger()->addMessage('Invalid callback request. Oops. Sorry.');
            return $this->redirect()->toUrl('/');
        }
    }

    public function processAction()
    {
        if ($this->getRequest()->isPost()) {
            $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");

            if ($sessionManager->getStorage()->flickr_access_token === null) {
                $this->flashMessenger()->addMessage('Please log in Flickr');
                return $this->redirect()->toRoute('application');
            }

            return $this->_process();
        } else {
            return $this->redirect()->toRoute('application');
        }
    }

    protected function _prepareParamsForProcess($type, $userId, $page, $photoSetId = null)
    {
        switch ($type) {
            case 'by-taken-year':
                $year = $this->getRequest()->getPost('year');
                $params = array(
                    'user_id' => $userId,
                    'per_page' => 500,
                    'page' => $page,
                    'min_taken_date' => mktime(0, 0, 0, 1, 1, $year),
                    'max_taken_date' => mktime(0, 0, 0, 1, 1, $year + 1),
                    'media' => 'photo',
                    'extras' => 'date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'
                );
                break;

            case 'by-tags':
                $tags = $this->getRequest()->getPost('tags');
                $params = array(
                    'user_id' => $userId,
                    'per_page' => 500,
                    'page' => $page,
                    'tags' => implode(',', $tags),
                    'media' => 'photo',
                    'extras' => 'date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'
                );
                break;

            case 'by-set':
                $params = array(
                    'user_id' => $userId,
                    'photoset_id' => $photoSetId,
                    'per_page' => 500,
                    'page' => $page,
                    'media' => 'photo',
                    'extras' => 'date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'
                );
                break;

            default:
                $params = array();
                break;
        }

        return $params;
    }

    protected function _process()
    {
        set_time_limit(300);

        $view = array();

        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");
        $token = unserialize($sessionManager->getStorage()->flickr_access_token);
        /* @var $token \ZendOAuth\Token\Access */

        $_startTime = microtime(true);

        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $flickr = new Itscaro\Service\Flickr\Flickr($this->_config['flickr']['oauth'], $config);
        $flickr->setAccessToken($token);

        // Instantiate a client object
        $flickrMulti = new Itscaro\Service\Flickr\ClientMulti('https://api.flickr.com/services/rest/', $this->_config['flickr']['oauth'], $config);
        $flickrMulti->setAccessToken($token);

        $type = $this->getRequest()->getPost('type');

        $page = 1;
        $pages = null;
        while ($page <= $pages || $pages === null) {
            //@todo analyse $response['stat'] $response['code'] $response['message']);

            switch ($type) {
                case 'by-set':
                    $photosetId = $this->getRequest()->getPost('set');

                    $params = $this->_prepareParamsForProcess($type, $token->getParam('user_nsid'), $page, $photosetId);

                    $this->_logger->info('> flickr.photosets.getPhotos', $params);

                    $response = $flickrMulti->dispatch('GET', 'flickr.photosets.getPhotos', $params);
                    $response = json_decode($response, true);
                    /* @var $photos Itscaro\Service\Flickr\Model\Photo\PhotoCollection */
                    if (!isset($photos)) {
                        $photos = new \Itscaro\Service\Flickr\Model\Photo\PhotoCollection($response['photoset']);
                    }
                    $photos->addItems($response['photoset']['photo']);

                    $this->_logger->info('< flickr.photosets.getPhotos : ' . count($response['photoset']['photo']) . ' found');
                    unset($response);
                    break;

                default:
                    $params = $this->_prepareParamsForProcess($type, $token->getParam('user_nsid'), $page);

                    $this->_logger->info('> flickr.photos.search', $params);

                    $response = $flickrMulti->dispatch('GET', 'flickr.photos.search', $params);
                    $response = json_decode($response, true);
                    /* @var $photos Itscaro\Service\Flickr\Model\Photo\PhotoCollection */
                    if (!isset($photos)) {
                        $photos = new \Itscaro\Service\Flickr\Model\Photo\PhotoCollection($response['photos']);
                    }
                    $photos->addItems($response['photos']['photo']);

                    $this->_logger->info('< flickr.photos.search : ' . count($response['photos']['photo']) . ' found');
                    unset($response);
                    break;
            }

            $page++;
            if ($pages === null) {
                $pages = $photos->pages;
            }
            $photosPerPage[$page] = $photos;
        }
        $this->_logger->info('$photos->total : ' . $photos->total);
        $view['foundPhotos'] = $photos->total;

        if ($photos->total) {
            // Thumbnail Hash
            $photosByHash = array();
            $md5 = array();
            $this->_processThumbnailHash($flickrMulti->getRestClient(), $photos, $photosByHash, $md5);

            // Get Exifs of found photos
            $photosByExifHash = array();
            $photosByTitle = array();
            $consolidatedExif = array();
            $this->_processExif($flickr, $photos, $photosByExifHash, $photosByTitle, $consolidatedExif);

            foreach ($photosByHash as $_photoByHash) {
                if (count($_photoByHash) > 1) {
                    foreach ($_photoByHash as $_photo) {
                        $view['photosByHash'][$_photo->id]['photoUrl'] = sprintf('https://www.flickr.com/photos/%s/%s/', $_photo->pathalias, $_photo->id);
                        $view['photosByHash'][$_photo->id]['photo'] = $_photo;
                        $view['photosByHash'][$_photo->id]['exif'] = $consolidatedExif[$_photo->id];
                        $view['photosByHash'][$_photo->id]['md5'] = $md5[$_photo->id];
                    }
                }
            }

            foreach ($photosByExifHash as $_photoByHash) {
                if (count($_photoByHash) > 1) {
                    foreach ($_photoByHash as $_photo) {
                        $view['photosByExif'][$_photo->id]['photoUrl'] = sprintf('https://www.flickr.com/photos/%s/%s/', $_photo->pathalias, $_photo->id);
                        $view['photosByExif'][$_photo->id]['photo'] = $_photo;
                        $view['photosByExif'][$_photo->id]['exif'] = $consolidatedExif[$_photo->id];
                        $view['photosByExif'][$_photo->id]['md5'] = $md5[$_photo->id];
                    }
                }
            }

            foreach ($photosByTitle as $_photoByTitle) {
                if (count($_photoByTitle) > 1) {
                    foreach ($_photoByTitle as $_photo) {
                        $view['photosByTitle'][$_photo->id]['photoUrl'] = sprintf('https://www.flickr.com/photos/%s/%s/', $_photo->pathalias, $_photo->id);
                        $view['photosByTitle'][$_photo->id]['photo'] = $_photo;
                        $view['photosByTitle'][$_photo->id]['exif'] = $consolidatedExif[$_photo->id];
                        $view['photosByTitle'][$_photo->id]['md5'] = $md5[$_photo->id];
                    }
                }
            }
        }

        $_endTime = microtime(true);

        $view['execTime'] = $_endTime - $_startTime;
        $view['memory'] = memory_get_peak_usage(true);

        return new ViewModel($view);
    }

    public function ajaxAction()
    {
        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");

        if ($sessionManager->getStorage()->flickr_access_token === null) {
            $this->flashMessenger()->addMessage('Please log in Flickr');
            $this->redirect()->toUrl('/');
        }

        $method = $this->getRequest()->getPost('method');

        $json = array();

        switch ($method) {
            case 'add-tag-delete':
                $json['state'] = 0;
                $json['response'] = $this->_ajaxAddTagDelete();
                break;

            default:
                $json['state'] = 1;
                break;
        }

        return new JsonModel($json);
    }

    protected function _ajaxAddTagDelete()
    {
        $sessionManager = $this->getServiceLocator()->get("Zend\Session\SessionManager");

        $token = unserialize($sessionManager->getStorage()->flickr_access_token);
        /* @var $token \ZendOAuth\Token\Access */
        $args = $this->getRequest()->getPost('args');

        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'sslverifypeer' => false
        );

        // Instantiate a client object
        $flickrMulti = new Itscaro\Service\Flickr\ClientMulti('https://api.flickr.com/services/rest/', $this->_config['flickr']['oauth'], $config);
        $flickrMulti->setAccessToken($token);

        $this->_logger->info('> dispatchMulti: ' . count($args) . " requests");
        $_startTimeDispatchMulti = microtime(true);
        foreach ($args as $_arg) {
            $requestIds[$_arg['value']] = $flickrMulti->addToQueue('POST', 'flickr.photos.addTags', array(
                'photo_id' => $_arg['value'],
                'tags' => 'delete'
            ));
        }
        $response = $flickrMulti->dispatchMulti();

        // Associcate reponse to photo

        $flippledRequestIds = array_flip($requestIds);
        foreach ($response as $_key => $_reponse) {
            $finalResponse[$flippledRequestIds[$_key]] = $_reponse;
        }
        $_endTimeDispatchMulti = microtime(true);
        $this->_logger->info('< dispatchMulti', array($_endTimeDispatchMulti - $_startTimeDispatchMulti));

        return $finalResponse;
    }

    /**
     *
     * @return \Application\Model\UserTable
     */
    public function getUserTable()
    {
        return $this->getServiceLocator()->get('Application\Model\UserTable');
    }

    /**
     * 
     * @param Itscaro\Service\Flickr\Flickr $client
     * @param Itscaro\Service\Flickr\Model\Photo\PhotoCollection $photos
     * @param array $photosByExifHash
     * @param array $photosByTitle
     * @param array $consolidatedExif
     */
    protected function _processExif(Itscaro\Service\Flickr\Flickr $client, Itscaro\Service\Flickr\Model\Photo\PhotoCollection $photos, array &$photosByExifHash, array &$photosByTitle, array &$consolidatedExif)
    {
        $i = 0;
        $photoPerBatch = 250;
        do {
            $this->_logger->info("> Getting exifs: flickr.photos.getExif");

            $requestIds = array();
            $_photos = array_slice($photos->photo, $i * $photoPerBatch, $photoPerBatch);
            foreach ($_photos as $_photo) {
                $requestIds[$_photo->id] = $client->photoGetExif($_photo->id);
            }

            $this->_logger->info('> dispatchMulti: ' . count($requestIds) . " requests");
            $_startTimeDispatchMulti = microtime(true);
            $responses = $client->dispatch();
            $_endTimeDispatchMulti = microtime(true);
            $this->_logger->info('< dispatchMulti: Executed in ' . ($_endTimeDispatchMulti - $_startTimeDispatchMulti));

            foreach ($responses as $_photoExif) {
                if (isset($_photoExif['photo'])) {
                    $exifs = new \Itscaro\Service\Flickr\Model\Photo\ExifCollection($_photoExif['photo']);
                    $exifs->addItems($_photoExif['photo']['exif']);

                    $exifsOfPhotos[$exifs->id] = $exifs;
                }
                unset($_photoExif);
            }
            unset($responses);
            $this->_logger->info("< Getting exifs. Memory peak: " . memory_get_peak_usage(1) / 1024 / 1024);

            //temporisation
            usleep(100);
            $i++;
        } while (ceil(count($photos->photo) / $photoPerBatch) > $i);

        $exifToUse = array(
            'ExifIFD:ExposureTime',
            'ExifIFD:FNumber',
            'ExifIFD:ISO',
            'ExifIFD:FocalLength',
            'ExifIFD:DateTimeOriginal',
            'ExifIFD:FocalLengthIn35mmFormat',
            'IFD0:Model',
            'IFD0:Orientation',
        );

        foreach ($exifsOfPhotos as $exifsKey => $exifs) {
            $photosByTitle[$photos->photo[$exifs->id]->title][] = $photos->photo[$exifs->id];
            $consolidatedExif[$exifs->id] = array();

            if (count($exifs->exif) == 0) {
                continue;
            }

            foreach ($exifs->exif as $_exif) {
                $_key = $_exif->tagspace . ':' . $_exif->tag;
                if (in_array($_key, $exifToUse)) {
                    $consolidatedExif[$exifs->id][$_key] = $_exif->raw['_content'];
                }
            }
            if (count($consolidatedExif[$exifs->id]) > 0) {
                ksort($consolidatedExif[$exifs->id]);
                $consolidatedExif[$exifs->id]['_hash'] = md5(serialize($consolidatedExif[$exifs->id]));
                $photosByExifHash[$consolidatedExif[$exifs->id]['_hash']][] = $photos->photo[$exifs->id];
            }

            $exifsOfPhotos[$exifsKey] = null;
            unset($exifsOfPhotos[$exifsKey]);
        }
        unset($exifsOfPhotos);
    }

    /**
     * 
     * @param Itscaro\Rest\ClientMulti $client
     * @param Itscaro\Service\Flickr\Model\Photo\PhotoCollection $photos
     * @param array $photosByHash
     * @param array $md5
     */
    protected function _processThumbnailHash(Itscaro\Rest\ClientMulti $client, Itscaro\Service\Flickr\Model\Photo\PhotoCollection $photos, array &$photosByHash, array &$md5)
    {
        foreach ($photos->photo as $_photo) {
            $photoUrls[$_photo->id] = $_photo->url_t;
        }

        $logger = $this->_logger;

        // Add callback to client
        $client->setCallbackWriteFunction(function($ch, $chunk) use(&$data, $logger) {
            foreach ($data as &$d) {
                if ($ch === $d['curl']) {
                    //$logger->debug("CURLOPT_WRITEFUNCTION : chunk size " . strlen($chunk));
                    hash_update($d['hash'], $chunk);
                    break;
                }
            }
        });

        $i = 0;
        $photoPerBatch = 500;
        do {
            $data = array();

            $urls = array_slice($photoUrls, $i * $photoPerBatch, $photoPerBatch, true);
            $this->_logger->info('> Gettings thumbnails : ' . count($urls));

            foreach ($urls as $id => $url) {
                // Add to queue
                $requestId = $client->get($url);

                $data[$id] = array(
                    'requestId' => $requestId,
                    'url' => $url,
                    'curl' => $client->getHandler($requestId),
                    'hash' => hash_init('md5'),
                );
            }

            $client->dispatch();

            foreach ($data as $_id => $_data) {
                $requestInfo = $client->getInfo($_data['requestId']);

                $md5[$_id] = hash_final($_data['hash'], false);
                $this->_logger->debug("ID: {$_id} | MD5: {$md5[$_id]} | URL: {$_data['url']}");
                $photosByHash[$md5[$_id]][] = $photos->photo[$_id];

                if ($requestInfo['http_code'] !== 200) {
                    $this->_logger->err("- Gettings thumbnails", $requestInfo);
                }
                unset($data[$_id]);
            }
            $data = array();

            $this->_logger->info('< Gettings thumbnails. $photosByHash : ' . count($photosByHash) . " | Memory peak: " . memory_get_peak_usage(1) / 1024 / 1024);

            //temporisation
            usleep(250);
            $i++;
        } while (ceil(count($photoUrls) / $photoPerBatch) > $i);

        // Remove callback from client
        $client->removeCallbackWriteFunction();
    }

}
