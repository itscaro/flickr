<?php

/**
 * Description of ControllerAbstract
 *
 * @author Minh-Quan
 */

namespace Application\Controller;

use Zend;
use Zend\Mvc\Controller\AbstractActionController as ZendAbstractActionController;

class AbstractActionController extends ZendAbstractActionController {

    protected $_config;

    /**
     *
     * @var Zend\Log\Logger
     */
    protected $_logger;

    public function __construct()
    {
        
    }

    protected function attachDefaultListeners()
    {
        parent::attachDefaultListeners();

        $event_mgr = $this->getEventManager();
        $event_mgr->attach('dispatch', array($this, 'preDispatch'), 100);
        $event_mgr->attach('dispatch', array($this, 'postDispatch'), -100);
    }

    public function preDispatch(Zend\Mvc\MvcEvent $e)
    {
        $this->_config = $this->getServiceLocator()->get("Config");

        $this->_logger = $this->getServiceLocator()->get('Log\App');
        $this->_logger->addProcessor(new \Application\Log\Processor\Memory());
    }

    public function postDispatch(Zend\Mvc\MvcEvent $e)
    {
        
    }

}
