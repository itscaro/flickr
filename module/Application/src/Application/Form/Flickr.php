<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Description of Flickr
 *
 * @author Minh-Quan
 */
class Flickr extends Form {

    function __construct($name = 'flickr')
    {
        parent::__construct($name);
        $this->setAttributes(array(
            'method' => 'post',
            'role' => 'form'
        ));

        $this->add(array(
            'name' => 'type',
            'type' => 'Radio',
            'options' => array(
                'value_options' => array(
                    'by-taken-year' => 'By Taken Year',
                    'by-tags' => 'By Tags',
                    'by-set' => 'By Set',
                ),
            ),
        ));

        $values = array();
        for ($i = date('Y'); $i >= 1970 ; $i--) {
            $values[$i] = $i;
        }
        $this->add(array(
            'name' => 'year',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'by-taken-year',
                'class' => 'process-type',
            ),
            'options' => array(
                'label' => 'Year',
                'value_options' => $values,
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'by-tags',
                'class' => 'process-type',
                'style' => 'height: 300px',
                'multiple' => 'multiple',
            ),
            'options' => array(
                'label' => 'Tags',
            ),
        ));

        $this->add(array(
            'name' => 'set',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'by-set',
                'class' => 'process-type',
                //'style' => 'height: 300px',
                //'multiple' => 'multiple',
            ),
            'options' => array(
                'label' => 'Sets',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'id' => 'submitbutton',
                'class' => 'btn btn-default',
                'value' => 'Go',
            ),
        ));
    }

}
