<?php

namespace Application\Log\Processor;

use Zend\Log\Processor;

class Memory implements Processor\ProcessorInterface {

    /**
     * Adds an identifier for the request to the log.
     *
     * This enables to filter the log for messages belonging to a specific request
     *
     * @param array $event event data
     * @return array event data
     */
    public function process(array $event)
    {
        if (!isset($event['extra'])) {
            $event['extra'] = array();
        }

        $event['extra']['memory'] = array(
            'current' => memory_get_usage(1) / 1024 / 1024,
            'peak' => memory_get_peak_usage(1) / 1024 / 1024,
        );

        return $event;
    }

}
