<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * Description of User
 *
 * @author itscaro
 */
class UserTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSetl
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     *
     * @param int $id
     * @return User
     * @throws \Exception
     */
    public function get($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     *
     * @param string $user_nsid
     * @return User
     * @throws \Exception
     */
    public function getByUserNsid($user_nsid)
    {
        $rowset = $this->tableGateway->select(array('user_nsid' => $user_nsid));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with user_nsid = $user_nsid");
        }
        return $row;
    }

    /**
     *
     * @param User $user
     * @throws \Exception
     */
    public function save(User $user)
    {
        $data = array(
            'user_nsid' => $user->user_nsid,
            'username' => $user->username,
            'fullname' => $user->fullname,
            'oauth_token' => $user->oauth_token,
            'oauth_token_secret' => $user->oauth_token_secret,
            'meta' => $user->meta,
        );

        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->get($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User does not exist');
            }
        }
    }

    /**
     *
     * @param int $id
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

}
