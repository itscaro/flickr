<?php

namespace Application\Model;

/**
 * Description of User
 *
 * @author itscaro
 */
class User
{

    public $id;
    public $user_nsid;
    public $username;
    public $fullname;
    public $oauth_token;
    public $oauth_token_secret;
    public $meta;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_nsid = (!empty($data['user_nsid'])) ? $data['user_nsid'] : null;
        $this->username = (!empty($data['username'])) ? $data['username'] : null;
        $this->fullname = (!empty($data['fullname'])) ? $data['fullname'] : null;
        $this->oauth_token = (!empty($data['oauth_token'])) ? $data['oauth_token'] : null;
        $this->oauth_token_secret = (!empty($data['oauth_token_secret'])) ? $data['oauth_token_secret'] : null;
        $this->meta = (!empty($data['meta'])) ? $data['meta'] : null;
    }

}
