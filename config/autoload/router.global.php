<?php

/**
 * Route names
 * 
 * application
 * application/default
 * application/process/by-year
 */
return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'process' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => 'flickr/process',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Flickr',
                                'action' => 'process',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'by-year' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/year[/:year]',
                                    'constraints' => array(
                                        'year' => '[0-9]*'
                                    ),
                                    'defaults' => array(
                                    )
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
