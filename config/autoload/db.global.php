<?php

return array(
    'db' => array(
        'driver' => 'Pdo_Sqlite',
        'database' => dirname(dirname(__DIR__)) . '/data/db.sqlite'
    ),
);
