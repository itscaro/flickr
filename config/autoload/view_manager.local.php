<?php

/**
 * Route names
 * 
 * application
 * application/default
 * application/process/by-year
 */
return array(
    'view_manager' => array(
        'display_exceptions' => true,
    ),
);
